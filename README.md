---
license: cc-by-nc-4.0
language:
- en
library_name: diffusers
pipeline_tag: text-to-image
tags:
- art
---
# MergeStove2.5D（融合炉2.5D）

**Hatsune Miku, Thank you.**  
It's time to say goodbye to MergeStove, sayolala. Thanks for your sincerely surpport. The **MK8** maybe the last MergeStove, and if I have enough time, I will reconstruct this Readme, including the previews of MK8.   
是时候和MergeStove说再见了，感谢你们的陪伴。**MK8**可能会是最后一个MergeStove模型了，如果我有时间，我会把现在的Readme重构的，包括补上MK8的预览图。

MK7 is ready!!! In memory of my college entrance exam a total year ago. For previews, ALL here for MK7, just download and enjoy it. :)    
MK7版本已发布，纪念一年前我的高考。预览图已补充，下载它，你会喜欢它的。:)

**Important** Use the negatives below for best performance of MK7. Other options are also available in the Selected Negative Prompts for MK7.txt  
*badhandv4, EasyNegative, verybadimagenegative_v1.3,illustration, 3d, sepia, painting, cartoons, sketch, (worst quality:1.74), (low quality:1.74), (normal quality:1.44), lowres, bad anatomy, normal quality, ((monochrome)), ((grayscale)), ((letters)), ((english)), capital*  
It contains 3 negative textural embeddings, which are **badhandv4, EasyNegative, verybadimagenegative_v1.3**, each of them can easily download on huggingface.  
**重要** 使用上面的负面描述词以使MK7达到最佳效果。其他的可选负面描述词可以在Selected Negative Prompts for MK7.txt内查看。  
它包含3个负面嵌入Embeddings，分别是**badhandv4, EasyNegative, verybadimagenegative_v1.3**，且每个都能轻松的在huggingface上下载到。

PS: MK5 and MK6 use these configs below will be much better.  
提示：MK5和MK6使用以下设置可能会更好。  
*Steps: 20, Sampler: Heun, CFG scale: 7, Denoising strength: 0.5, Clip skip: 2, Hires upscale: 3, Hires upscaler: R-ESRGAN 4x+ Anime6B, Used embeddings: EasyNegative [119b]*

**mk6 reconstructed** its base model, which change to AbyssOrangeMix2_sfw. And with models new to here, it expands its knowledges, and which be **impressive** in extra-big pictures. I hope you can love it!  
**mk6版更新重构了**它本身的基础模型，其中的AbyssOrangeMix2被更换为sfw版。还有我加入了很多新模型来扩展它的知识面，这使得mk6在超大图片中表现**惊艳**。

mk5 update, specially for **chinese friends**, quite a few improvements.  
mk5版更新，是专门为了**中国朋友们**准备的，有非常多的改进。

MergeStove2.5D is a **merge** stable diffusion model specialized in **anime**, which improves anatomy of anime characters, especially with **eyes** and **hands**, without losing anime objects (like substances or charaters).  
Much better for working at 0.9K-1.2K resoultion, or use Hires.fix instead. In another words, before Hires.fix, long side at 0.9k-1.2k, short side at 0.5k-0.7k resolutions are better.  
Provide in 6 versions. Personally mk1 works better, but mk2 give out more vivid pictures. Previous update mk3 and mk4 are proudly do better in 2.5D figures. mk3 do better in generate body, but mk4 improve scene.  
融合炉2.5D是一个**动漫风格特化**的稳定扩散模型，由**多个模型融合**而来，专门改善动漫人物的身体结构，特别是**眼睛**和**手**，同时不会丢失任何动漫中的对象（物体、人物等）。  
其在900-1200像素的分辨率下工作较好，或者可以使用高清修复改善其高分辨率表现。换句话说，高清修复前长边900-1200像素，短边500-700像素这样子比较好。  
提供6个版本。个人感觉mk1版工作的更好，但是mk2版本能生成更生动的图像。我可以很自豪的说，先前更新的mk3和mk4在2.5D人物中表现的更好。mk3有相对较好的人体，但是mk4改进了景物表现。

**No commercial usage! 严禁商用！**


# Preview（预览）

**Updates**  
**mk7** (after hi-res fix at 0.45)（高清修复比率0.45） *demon tail, butterfly, tail, bug, 1girl, long hair, wristband, shoes, hatsune miku, shirt, choker, black legwear, aqua hair, bike shorts, solo, blue butterfly, twintails, black choker, bracelet, full body, black ribbon, cow tail, very long hair, tail ornament, jewelry, black bow, hair between eyes, ahoge, white shirt, earrings, grey background, tail bow, standing, jacket, shorts, collarbone, off shoulder, short sleeves, ribbon, black footwear, aqua eyes, gradient, bow, socks, looking at viewer*  
![27720-1671519421.png](https://s3.amazonaws.com/moonup/production/uploads/6384c33038f4aec99c4a7483/DQ77sM5KG_9taJdJpyFSw.png)  
**mk7** (after hi-res fix at 0.45)（高清修复比率0.45） *{masterpiece}, hatsune miku, sit on sakura tree branch, floating cyan long hair, wind flow, sakura petals floating, closed eyes, sun shine upward, shadows,white long dress, cloud sky with sun, hamony and peace, bare feet, medium breast*  
![27754-263900082.png](https://s3.amazonaws.com/moonup/production/uploads/6384c33038f4aec99c4a7483/AnAa-gjxMk5jplMyDLF7p.png)  
**mk7** (after hi-res fix at 0.45)（高清修复比率0.45） *flying sweatdrops, long hair, blue hair, hair ornament, 1girl, english text, open mouth, closed eyes, phone, smile, cellphone, uniform, necktie, gloves, bangs, solo, blush, hatsune miku*  
![27764-2380869904.png](https://s3.amazonaws.com/moonup/production/uploads/6384c33038f4aec99c4a7483/BskMrzcDXu_G6lcC5bRaL.png)

**Previous**  
**mk6** (after hi-res fix at 0.6)（高清修复比率0.6） *close-up, upper body, blue eyes black middle, snow miku stand in right side of frame, starry night with distance snow mountains scene in left side of frame, solo charater, snow stage, thick coat long dress, shinny and vivid eyes, curly long aqua hair fall on ground, medium breasts, windless, floating snows, mountain right, snow forest*  
![19824-2561960886.png](https://s3.amazonaws.com/moonup/production/uploads/1678274004231-6384c33038f4aec99c4a7483.png)  
**mk6** (after hi-res fix at 0.6)（高清修复比率0.6） *halo, [wings], leg tie, (hathatsune) miku, full body, long legs, [[lips]], red eyes, medium breasts, (white hair), (streaked blue) hair, round face, [ahoge], black gloves, (hathatsune) miku, closed mouth, full body, straight long 2 legs, starry night, bubble nebula,, [[lips]], lace long dress, small breasts, flat chest, flowers*  
![19806-516847586.png](https://s3.amazonaws.com/moonup/production/uploads/1678273348478-6384c33038f4aec99c4a7483.png)  
**mk6** (after hi-res fix at 0.6)（高清修复比率0.6） *solo, halo, feather wings, (hathatsune) miku, fox ears, straight long 2 legs, black long silk stocking, leg ring tie, full body, [[lips]], red eyes, medium breasts, ahoge, (white hair), (streaked blue) hair, round face, black gloves, closed mouth, starry night, bubble nebula, lace long dress, medium breasts, feathers*  
![19802-2257762706.png](https://s3.amazonaws.com/moonup/production/uploads/1678273417106-6384c33038f4aec99c4a7483.png)  
**mk5** (after hi-res fix at 0.7)（高清修复比率0.7） *(masterpiece), (((a girl))), ((hatsune miku)), (smiling), ((shining red medium eyes)), medium breasts, pink lips, moon in the sky, dark night, blue flowers surround one's, (blue dress), (blue long hair), stars shining, green grassland, (stream in grassland), (one's stand in the grassland), face to viewer, black higheels, long legs, full body*  
![17048-50394498.png](https://s3.amazonaws.com/moonup/production/uploads/1677494436744-6384c33038f4aec99c4a7483.png)  
**mk5** (after hi-res fix at 0.6)（高清修复比率0.6） *hatsune miku, closed mouth, full body, straight long legs, starry night, bubble nebula,, [[lips]], black long dress*  
![17406-2132407032.png](https://s3.amazonaws.com/moonup/production/uploads/1677494356421-6384c33038f4aec99c4a7483.png)  
**mk1** (after hi-res fix at 0.7)（高清修复比率0.7） *miku, ruby eyes, face to viewer, solo, medium breasts, soft light, outdoors, garden, seaside, beauty*  
![12848-2569952539.png](https://s3.amazonaws.com/moonup/production/uploads/1674739809930-6384c33038f4aec99c4a7483.png)  
**mk1** *miku, crystal eyes, upper body, face to viewer, solo, medium breasts, soft light, garden, seaside, ocean, bikini*  
![12908-2604523401.png](https://s3.amazonaws.com/moonup/production/uploads/1674741158723-6384c33038f4aec99c4a7483.png)  
**mk1** *miku, crystal eyes, upper body, face to viewer, solo, medium breasts, soft light, outdoors, garden, seaside, beauty, blue white dress*  
![12867-1973802650.png](https://s3.amazonaws.com/moonup/production/uploads/1674742488916-6384c33038f4aec99c4a7483.png)  
**mk2** *miku, crystal eyes, upper body, face to viewer, solo, before bookshelf, book in hands*  
![12947-375827774.png](https://s3.amazonaws.com/moonup/production/uploads/1674743285611-6384c33038f4aec99c4a7483.png)  
**mk2** *miku, crystal eyes, upper body, face to viewer, solo, before bookshelf, book in hands*  
![12948-2412520204.png](https://s3.amazonaws.com/moonup/production/uploads/1674743452171-6384c33038f4aec99c4a7483.png)  
**mk2** *miku, crystal eyes, upper body, face to viewer, solo, before bookshelf, book in hands*  
![12949-2375419362.png](https://s3.amazonaws.com/moonup/production/uploads/1674743472389-6384c33038f4aec99c4a7483.png)  
**mk3** (after hi-res fix at 0.7)（高清修复比率0.7） *hathatsune miku, seaside, shinny eyes, medium breasts, garden, ocean, seawind, soft sunset, beauty, beach shoes, short dress*  
![14986-2841316978.png](https://s3.amazonaws.com/moonup/production/uploads/1675502733159-6384c33038f4aec99c4a7483.png)  
**mk3** (after hi-res fix at 0.7)（高清修复比率0.7） *miku, seaside, shinny eyes, medium breasts, bikini, surfing, on surfing board, wave, seawind, (wet body:0.75), (🏄🏻:0.66)*  
![15086-1008675228.png](https://s3.amazonaws.com/moonup/production/uploads/1675515848039-6384c33038f4aec99c4a7483.png)  
**mk4** (after hi-res fix at 0.7)（高清修复比率0.7） *hathatsune miku, seaside, shinny eyes, medium breasts, garden, ocean, seawind, soft sunset, beauty, beach shoes, short dress*  
![14984-1376419686.png](https://s3.amazonaws.com/moonup/production/uploads/1675502762046-6384c33038f4aec99c4a7483.png)  
**mk4** (after hi-res fix at 0.7)（高清修复比率0.7） *miku, seaside, shinny eyes, medium breasts, bikini, bare feet, (surfing), (on 1_surfing_board), wave, seawind, wet body, liquid on cloth, see through*  
![15109-405782135.png](https://s3.amazonaws.com/moonup/production/uploads/1675515873124-6384c33038f4aec99c4a7483.png)  


# Usage（使用方法）

Use as normal stable diffusion model package v1.x, no external yaml config is needed.   
**Recommand settings: Steps: 9-28, Sampler: DPM++ SDE Karras, CFG scale: 5-16, Denoising strength: 0.6-0.7, Hires upscale: 2, Hires upscaler: Latent**  
用作正常的稳定扩散模型包v1.x，无需额外的YAML配置文件。  
**推荐设置：迭代步数：9-28，采样器：DPM++ SDE Karras，提示词相关性：5-16，去噪强度：0.6-0.7，高清修复放大倍率：2，高清修复放大器：Latent**


# Tags（描述词）

Positives as you like, maybe less quality words works better. You can get inspirations from upper descriptions.  
**Negatives better to use the basic prompts, or just replace as bad_prompt embedding.**  
**Negatives Example:** *(bad_prompt), cleavage, lowres, bad anatomy, bad hands, text, error, missing fingers, extra digit, fewer digits, cropped, worst quality, low quality, normal quality, jpeg artifacts, signature, watermark, username, blurry, artists name*  

正面填写你喜欢的描述词，也许更少的质量描述词能使其工作的更好。你可以在上面的预览图描述词中得到灵感。  
**负面描述词最好用基本负面，或者简单的把它们替换成bad_prompt这个嵌入模型。**  
**负面描述词示例：** *(bad_prompt), cleavage, lowres, bad anatomy, bad hands, text, error, missing fingers, extra digit, fewer digits, cropped, worst quality, low quality, normal quality, jpeg artifacts, signature, watermark, username, blurry, artists name*

**Use "blue eyes black middle" description can get huge improvement on pupil at low resolution! Colors can change as your preferance.**  
**使用"blue eyes black middle"这样子的描述词可在低分辨率下极大的改善对瞳孔的描绘！颜色可以改为你喜欢的。**

Here are the **better negatives**, thanks andite: *lowres, ((bad anatomy)), ((bad hands)), text, missing finger, extra digits, fewer digits, blurry, ((mutated hands and fingers)), (poorly drawn face), ((mutation)), ((deformed face)), (ugly), ((bad proportions)), ((extra limbs)), extra face, (double head), (extra head), ((extra feet)), monster, logo, cropped, worst quality, low quality, normal quality, jpeg, humpbacked, long body, long neck, ((jpeg artifacts))*  
这里是**更好的负面描述词**，谢谢andite：*lowres, ((bad anatomy)), ((bad hands)), text, missing finger, extra digits, fewer digits, blurry, ((mutated hands and fingers)), (poorly drawn face), ((mutation)), ((deformed face)), (ugly), ((bad proportions)), ((extra limbs)), extra face, (double head), (extra head), ((extra feet)), monster, logo, cropped, worst quality, low quality, normal quality, jpeg, humpbacked, long body, long neck, ((jpeg artifacts))*

From NovelAI 中文频道, I got some **even better negative prompts**. That is it, *EasyNegative, paintings, sketches, (worst quality:2), (low quality:2), (normal quality:2), lowres, normal quality, ((monochrome)), ((grayscale)), skin spots, acnes, skin blemishes, age spot, glans, extra fingers, fewer fingers, strange fingers, ((bad hand)), Hand grip, (lean), Extra ears, (Four ears), Strange eyes, ((Bare nipple)), nsfw, (three arms), Many hands, (Many arms), ((watermarking)), (inaccurate limb:1.2)*  
Note, it use the **EasyNegative** embbedings, which you need to download manually. It is also a well working filter on nsfw contants.  
我在NovelAI 中文频道找到了一些**还要更好的负面描述词**。它们在这里， *EasyNegative, paintings, sketches, (worst quality:2), (low quality:2), (normal quality:2), lowres, normal quality, ((monochrome)), ((grayscale)), skin spots, acnes, skin blemishes, age spot, glans, extra fingers, fewer fingers, strange fingers, ((bad hand)), Hand grip, (lean), Extra ears, (Four ears), Strange eyes, ((Bare nipple)), nsfw, (three arms), Many hands, (Many arms), ((watermarking)), (inaccurate limb:1.2)*  
注意，它使用了**EasyNegative**这个嵌入模型，你需要手动下载它。这些描述词还能更好的过滤成人内容。

# Bias（不足）

**Notice:** Definitely important to enable the **Hires.fix**, especially on the **mk5 and mk6**. Or low quality images will be generated!!!  
**注意：** 启用**高清修复**至关重要，特别是在**mk5和mk6**上。不然会产生低质量图片！！！

**include nsfw contents due to its original models!**  
**DO NOT USE your generated pictures for Pirate human artists or any Internet Violence! Such as on Bilibili or Youtube.**  
Sometimes long necks appear. Still hazy a bit. Under some theme will produce wrong skin gloss. Sometimes overfitting. Often produce Unhuman Size Breasts girl pictures unless use cleavage tag in negative.  
**含有成人内容，由于其原始模型本身的不足！**  
**请勿把你用本模型生成的图像用于嘲讽人类画师或者其他任何形式的网络暴力！例如在Bilibili或者Youtube上。**  
有时会生成过长的脖子。仍然有点模糊。在某些特定场景会产生错误的皮肤光泽。有时生成的图像会过拟合训练集内版权图片。经常会生成非人类大小的乳房（USB）的女性图片，除非在负面描述词中使用cleavage这个标签。


# Formula（融合配方）

**Round1** animefull-latest(NovelAI)+64in1(Private, from a Chinese AI community NovelAI 中文频道) sum rate0.4  
**Round2** ()+AbyssOrangemix2_nsfw(WarriorMama777) sum rate0.2  
After baked in vae-ft-mse-840000-ema-pruned(StabilityAI) VAE, pruned ema, compressed to FP16, get MergeStove2.5D_mk1.  
**第一轮** animefull-latest(NovelAI)+64in1(私有，来自中国AI社区NovelAI 中文频道) 加权和模式 比率0.4  
**第二轮** ()+AbyssOrangemix2_nsfw(WarriorMama777) 加权和模式 比率0.2  
嵌入vae-ft-mse-840000-ema-pruned(StabilityAI)这个VAE模型后，去掉EMA权重，压缩为FP16格式，得到MergeStove2.5D_mk1模型。

**Round3A** MergeStove2.5D_mk1+Anmokomergetest1(Private, from a Chinese AI community NovelAI 中文频道, Download [Anmokomergetest1](https://huggingface.co/LieDeath/Anmokomergetest1).) sum rate0.4  
After baked in vae-ft-mse-840000-ema-pruned(StabilityAI) VAE, pruned ema, compressed to FP16, get MergeStove2.5D_mk2.  
**第三轮A** MergeStove2.5D_mk1+Anmokomergetest1(私有，来自中国AI社区NovelAI 中文频道，下载[Anmokomergetest1](https://huggingface.co/LieDeath/Anmokomergetest1)。) 加权和模式 比率0.4  
嵌入vae-ft-mse-840000-ema-pruned(StabilityAI)这个VAE模型后，去掉EMA权重，压缩为FP16格式，得到MergeStove2.5D_mk2模型。

**Round3B** MergeStove2.5D_mk1+uberRealisticPornMer_urpMv11(Civitai, from saftle) sum rate 0.1  
After baked in vae-ft-mse-840000-ema-pruned(StabilityAI) VAE, pruned ema, compressed to FP16, get MergeStove2.5D_mk3.  
**第三轮B** MergeStove2.5D_mk1+uberRealisticPornMer_urpMv11(来自CivitAI的saftle) 加权和模式 比率0.1  
嵌入vae-ft-mse-840000-ema-pruned(StabilityAI)这个VAE模型后，去掉EMA权重，压缩为FP16格式，得到MergeStove2.5D_mk3模型。

**Round4B** MergeStove2.5D_mk3+momoko-e(Anonymous) sum rate 0.1  
**Round5B** ()+Protogen_V2.2(darkstorm2150) sum rate 0.1  
After baked in vae-ft-mse-840000-ema-pruned(StabilityAI) VAE, pruned ema, compressed to FP16, get MergeStove2.5D_mk4.  
**第四轮B** MergeStove2.5D_mk3+momoko-e(匿名) 加权和模式 比率0.1  
**第五轮B** ()+Protogen_V2.2(darkstorm2150) 加权和模式 比率0.1  
嵌入vae-ft-mse-840000-ema-pruned(StabilityAI)这个VAE模型后，去掉EMA权重，压缩为FP16格式，得到MergeStove2.5D_mk4模型。

**Round4A** MergeStove2.5D_mk2+chilloutmix_Ni(Civitai, from tasuku) sum rate 0.1  
**Round5A** ()+laolei-new-berry-protogen mix(Civitai, from hokono) sum rate 0.1  
**Round6A** ()+pastelmix(andite) sum rate 0.05  
After baked in vae-ft-mse-840000-ema-pruned(StabilityAI) VAE, pruned ema, get MergeStove2.5D_mk5.  
**第四轮A** MergeStove2.5D_mk2+chilloutmix_Ni(来自CivitAI的tasuku) 加权和模式 比率0.1  
**第五轮A** ()+laolei-new-berry-protogen mix(来自CivitAI的hokono) 加权和模式 比率0.1  
**第六轮A** ()+pastelmix(andite) 加权和模式 比率0.05  
嵌入vae-ft-mse-840000-ema-pruned(StabilityAI)这个VAE模型后，去掉EMA权重，得到MergeStove2.5D_mk5模型。

**Special:** AbyssOrangemix2_sfw works better at all these above MergeStove2.5D series. Only Round6A works at FP32 mode.  
**注意:** AbyssOrangemix2_sfw在上面所有的MergeStove2.5D系列融合模型中工作的更好。只有第六轮A使用了FP32融合模式。

**Roundx** Replace AbyssOrangeMix2_nsfw with AbyssOrangeMix2_sfw and Reconstructed mk5 with full FP32, get modelx.  
**Round7x** modelx+Nothing-V0.3(Chinese, Anonymous) sum rate 0.1  
**Round8x** ()+7th_anime_v2_A(syaimu) sum rate 0.1  
**Round9x** ()+mdjrny-v4(Anonymous) mbw in4 rate 1  
After baked in vae-ft-mse-840000-ema-pruned(StabilityAI) VAE, pruned ema, get MergeStove2.5D_mk6.  
**第x轮** 把AbyssOrangeMix2_nsfw替换为AbyssOrangeMix2_sfw，然后用全FP32格式重构mk5，得到modelx。  
**第七轮x** modelx+Nothing-V0.3(来自中国，匿名) 加权和模式 比率0.1  
**第八轮x** ()+7th_anime_v2_A(syaimu) 加权和模式 比率0.1  
**第九轮x** ()+mdjrny-v4(Anonymous) MBW插件 仅调整in4层 比率1  
嵌入vae-ft-mse-840000-ema-pruned(StabilityAI)这个VAE模型后，去掉EMA权重，得到MergeStove2.5D_mk6模型。